#include <stdlib.h>
#include <stdio.h>
#include "../../src/musicplay.h"

#ifdef WIN32
#include <windows.h>
#else
#include <time.h>
#endif

//TODO: CHECK ERRORS!!!

//https://gist.github.com/rafaelglikis/ee7275bf80956a5308af5accb4871135
void sleep_ms(int milliseconds)
{
    #ifdef WIN32
        Sleep(milliseconds);
    #elif _POSIX_C_SOURCE >= 199309L
        struct timespec ts;
        ts.tv_sec = milliseconds / 1000;
        ts.tv_nsec = (milliseconds % 1000) * 1000000;
        nanosleep(&ts, NULL);
    #else
        usleep(milliseconds * 1000);
    #endif
}

void check_error(const char* info, int error)
{
	if(error != MusicPlayNoError)
	{
		fprintf(stderr, "ERROR: %s: %s\n", info, musicplay_strerror(error));
		exit(1);
	}
}

int main ()
{
	//print library version
	printf("Using %s\n", musicplay_version(NULL, NULL, NULL));

	int error;

	//init musicplay
	MusicPlayData* musicplay = musicplay_init(&error);
	check_error("Init musicplay", error);

	//select backend
	if(musicplay_check_backend(SoundIoBackendJack))
		musicplay_init_backend(musicplay, SoundIoBackendJack, &error);
	else if(musicplay_check_backend(SoundIoBackendPulseAudio))
		musicplay_init_backend(musicplay, SoundIoBackendPulseAudio, &error);
	else if(musicplay_check_backend(SoundIoBackendAlsa))
		musicplay_init_backend(musicplay, SoundIoBackendAlsa, &error);
	else if(musicplay_check_backend(SoundIoBackendWasapi))
		musicplay_init_backend(musicplay, SoundIoBackendWasapi, &error);
	else if(musicplay_check_backend(SoundIoBackendCoreAudio))
		musicplay_init_backend(musicplay, SoundIoBackendCoreAudio, &error);
	else
		musicplay_init_backend(musicplay, SoundIoBackendDummy, &error);
	check_error("Connect backend", error);

	//print all devices to select one
	int device_count = musicplay_get_device_count(musicplay, &error); //get device count
	check_error("Get device count", error);
	for(int i = 0; i < device_count; i++)
	{
		MusicPlayDeviceInfo info = musicplay_get_device_info(musicplay, i, &error); //load device info
		check_error("Get device info", error);

		int is_default = i == musicplay_get_default_output_device_index (musicplay);

		printf("%d) %s%s\n", i, info.name, is_default ? " (default)" : "");
		printf("\tIs RAW: %s\n", info.is_raw ? "true" : "false");
		printf("\tSoftware latency: %lf - %lf\n", info.software_latency_min, info.software_latency_max);
		printf("\tSample rate: %d - %d\n", info.samplerate_min, info.samplerate_max);

		musicplay_free_device_info(info);
	}

	//user select device
	int selected;
	printf("Select device: ");
	scanf("%d", &selected);

	//check user output
	if(selected < 0 || selected >= device_count)
	{
		fprintf(stderr, "Invalid device selected!\n");
		return 1;
	}

	//get output device info for stream config
	MusicPlayDeviceInfo info = musicplay_get_device_info(musicplay, selected, &error);

	//user select sample rate
	int samplerate;
	printf("Set sample rate (%d - %d): ", info.samplerate_min, info.samplerate_max);
	scanf("%d", &samplerate);

	if(samplerate < info.samplerate_min || samplerate > info.samplerate_max)
	{
		fprintf(stderr, "Invalid sample rate selected!\n");
		return 1;
	}

	//user select latency
	double latency;
	printf("Select latency (%lf - %lf): ", info.software_latency_min, info.software_latency_max);
	scanf("%lf", &latency);
	
	//some of backend set software latency to zero before open
	if(info.software_latency_max > 0 && (latency < info.software_latency_min || latency > info.software_latency_max))
	{
		fprintf(stderr, "Invalid latency selected!\n");
		return 1;
	}

	//init output device
	musicplay_init_device(musicplay, selected, &error);
	check_error("Init device", error);

	//init stream with max samplerate and latency
	musicplay_init_stream(musicplay, "sample_play (libmusicplay example)", samplerate, latency, &error);
	check_error("Init stream", error);

	//load audio (select SRC_SINC_FASTEST) and play
	musicplay_load_audio(musicplay, "music_orig.wav", 2, &error);
	check_error("Load audio", error);

	musicplay_play(musicplay, &error);
	check_error("Play audio", error);

	/*musicplay_seek(musicplay, 15, &error);
	check_error("Seek audio", error);

	while(!musicplay_end_of_file(musicplay, &error))
	{
		sleep_ms(100);
		printf("current time: %lf, total time: %lf\n\033[1A", musicplay_current_audio_time(musicplay, &error), musicplay_total_audio_time(musicplay, &error));
	}*/

	sleep_ms(1900);

	while(true)
	{
		sleep_ms(1900);
		musicplay_seek(musicplay, 2, &error);
		check_error("Seek audio", error);
		printf("seek, current_time: %lf\n", musicplay_current_audio_time(musicplay, &error));
		sleep_ms(1000);
		printf("postseek current_time: %lf\n", musicplay_current_audio_time(musicplay, &error));
		sleep_ms(1000);
		printf("postseek2 current_time: %lf\n", musicplay_current_audio_time(musicplay, &error));
	}

	//free device info
	musicplay_free_device_info(info);

	//destroy all musicplay components
	musicplay_destroy(musicplay);

	return 0;
}
