#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "types.h"
#include "config.h"

#ifdef WINDOWS
#include <windows.h>
#else
#include <time.h>
#endif

uint64_t monotonic_time ()
{
	#ifdef WINDOWS
//TODO: Check prepared

	LARGE_INTEGER frequency;
    LARGE_INTEGER time;
	QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&time);

	return (uint64_t)time.QuadPart * 1000 / (uint64_t)frequency.QuadPart;
	#else
	struct timespec time;
	clock_gettime(CLOCK_MONOTONIC_RAW, &time);
	return (uint64_t)time.tv_sec * 1000 + (uint64_t)time.tv_nsec / 1000000;
	#endif
}

char* duplicate_string (const char* str)
{
	size_t len = strlen(str);
	char* out = malloc(len + 1);
	memcpy(out, str, len);
	out[len] = '\0';
	return out;
}

bool check_prepared (void* memory, int* error)
{
	if(memory == NULL)
	{
		*error = MusicPlayNotPrepared;
		return false;
	}
	
	return true;
}

char* load_whole_file (const char* filename, size_t* size, int* error)
{
	*error = MusicPlayNoError;

	FILE* file = fopen(filename, "rb");
	if(file == NULL)
	{
		*error = MusicPlayFileNotExists;
		return NULL;
	}

	//get file size
	fseek(file, 0, SEEK_END);
	*size = ftell(file);
	rewind(file);

	//alloc
	char* data = malloc(*size);
	if(data == NULL)
	{
		*error = MusicPlayInvalidAlloc;
		return NULL;
	}

	//copy
	char* currentdata = data;
	size_t left = *size;
	while(left > 0)
	{
		size_t currentcopy = COPY_LENGTH;
		if(currentcopy > left)
			currentcopy = left;
		
		currentcopy = fread(currentdata, 1, currentcopy, file);
		if(currentcopy == 0)
		{
			*error = MusicPlayInvalidFile;
			free(data);
			return NULL;
		}
		left -= currentcopy;
		currentdata += currentcopy;
	}

	return data;
}
