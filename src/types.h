#ifndef __MUSICPLAY_TYPES__
#define __MUSICPLAY_TYPES__

#include <soundio.h>
#include <stdint.h>
#include <stdbool.h>
#include <samplerate.h>

#define SOUNDIO_ERROR_START 1000
#define SRC_ERROR_START 2000
#define WAV_ERROR_START 3000
#define OPUS_ERROR_START 4000
#define MP3_ERROR_START 5000

typedef enum musicplay_error
{
	//Sucess
	MusicPlayNoError,
	//Error with allocation libmusicplay variables
	MusicPlayInvalidAlloc,
	//Function is run before setup required component
	MusicPlayNotPrepared,
	//Device doesn't support any compatible format
	MusicPlayInvalidFormat,
	//File has unsupported format or it is broken
	MusicPlayInvalidFile,
	//No audio loaded (beware to enlace with MusicPlayNotPrepared)
	MusicPlayNoAudio,
	//Trying load audio from file, that doesn't exists
	MusicPlayFileNotExists
} MusicPlayError;

typedef enum musicplay_decoder_type
{
	MusicPlayWav,
	MusicPlayOpus,
	MusicPlayMp3
} MusicPlayDecoderType;

typedef long (*get_pcm_callback)(void* decoder, float* buffer, long buffer_frames, int* error);

typedef void (*audio_seek_callback)(void* decoder, long frame, int* error);

typedef void (*convert_format_callback)(void* value, float pcm);

//structure passed to audio write callback
typedef struct musicplay_stream_data
{
	//whole raw binary data
	char* filedata;
	//size of filedata
	size_t filesize;
	//audio sample rate
	int samplerate;
	//original frames in audio file (related to audio sample rate)
	long total_audio_frames;

	//resampler structure
	SRC_STATE* src;
	//output_sample_rate / input_sample_rate
	double resample_ratio;

	//type of decoder
	MusicPlayDecoderType decoder_type;
	//pointer to decoder struct
	void* decoder;
	//count of decoded frames (related to audio sample rate)
	long frames_decoded;

	//temporary buffer for decoded frames
	float* buffer;
	//number of frames that can be saved into buffer
	long buffer_frames;
	//internal buffer of decoded and resampled audio frames
	float* pcm;
	//number of frames that can be saved into pcm
	long pcm_frames;

	//position of first suitable frame in pcm
	long pcm_start;
	//number of remaining frames in pcm
	long pcm_left;

	//pointer to decoding function
	get_pcm_callback get_buffer_pcm;
	//pointer to decoder seek function
	audio_seek_callback audio_seek;
	//pointer to coverter function to correct format
	convert_format_callback convert_format;

	//count of played frames (related to stream output sample rate)
	long frames_played;
	//decoder reached the end of file
	bool end_of_file;

	//monotonic time in last stream_write callback if stream is not paused and difference monotonic time in last stream_write and pause time if stream is paused.
	uint64_t last_write_time;
	//stream is paused
	bool paused;

	//specified audio time to seek (if is 0 or more)
	double time_to_seek;
	//request monotonic time to seek audio
	uint64_t seek_request_time;
} MusicPlayStreamData;

typedef void (*decoder_init_callback)(MusicPlayStreamData* data, int stream_samplerate, int* error);

typedef void (*decoder_destroy_callback)(MusicPlayStreamData* data);

//structure with all requiered variables
typedef struct musicplay_data
{
	struct SoundIo* soundio;
	enum SoundIoBackend backend;
	struct SoundIoDevice* device;
	struct SoundIoOutStream* stream;
	MusicPlayStreamData stream_data;
} MusicPlayData;

//reduced device info passable to managed 
typedef struct musicplay_device_info
{
	int index;
	char* id;
	char* name;
	bool is_raw;
	double software_latency_min;
	double software_latency_max;
	int samplerate_min;
	int samplerate_max;
} MusicPlayDeviceInfo;

#endif