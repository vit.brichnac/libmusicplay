#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include <soundio.h>
#include <samplerate.h>
#include "types.h"
#include "config.h"
#include "tools.h"

#include "decoders/decoder_wav.h"

#ifdef MUSICPLAY_OPUS_ENABLE
#include "decoders/decoder_opus.h"
#endif

#ifdef MUSICPLAY_MP3_ENABLE
#include "decoders/decoder_mp3.h"
#endif

//return extension substring from original filename
static const char* get_extension (const char* filename)
{
	int length = strlen(filename);
	int dotpos = -1;
	for(int pos = length - 1; pos >= 0; pos--)
	{
		if(filename[pos] == '.')
		{
			dotpos = pos;
			break;
		}
	}

	return (filename + dotpos + 1);
}

static bool compare_insensetive(const char* a, const char* b)
{
	while(true)
	{
		char ca = *(a++);
		char cb = *(b++);

		if(ca == '\0' && cb == '\0')
			return true;
		if(tolower(ca) != tolower(cb))
			return false;
	}
}

const char* musicplay_decoder_strerror(int error)
{
	if(error > MP3_ERROR_START)
	#ifdef MUSICPLAY_MP3_ENABLE
		return decoder_mp3_strerror(error);
	#else
		return "MP3 decoder isn't enabled!";
	#endif

	if(error > OPUS_ERROR_START)
#ifdef MUSICPLAY_OPUS_ENABLE
		return decoder_opus_strerror(error);
#else
		return "Opus decoder isn't enabled!";
#endif

	if(error > WAV_ERROR_START)
		return decoder_wav_strerror(error);
	return "Invalid error number";
}

int get_pcm_and_resample (MusicPlayStreamData* data, int stream_samplerate)
{
	int error = 0;

	data->pcm_start = 0;
	data->pcm_left = 0;

	if(data->time_to_seek > 0)
	{
		data->time_to_seek += (double)(monotonic_time() - data->seek_request_time) / 1000.0;
		long seek_frame = data->samplerate * data->time_to_seek;
		if(seek_frame > data->total_audio_frames) //seek to end of file
		{
			data->end_of_file = true;
			return 0;
		}

		long play_frame = stream_samplerate * data->time_to_seek;

		data->audio_seek(data->decoder, seek_frame, &error);
		if(error != MusicPlayNoError)
			return error;

		src_reset(data->src);
		data->frames_decoded = seek_frame;
		data->frames_played = play_frame;
		data->end_of_file = false;
		
		data->time_to_seek = -1;
	}

	long frames = 0;
	if(data->get_buffer_pcm != NULL) //if audio is loaded
		frames = data->get_buffer_pcm(data->decoder, data->buffer, data->buffer_frames, &error);

	if(error != MusicPlayNoError)
		return error;
	
	if(frames <= 0)
	{
		data->pcm_left = 0;
		data->end_of_file = true;
		return 0;
	}

	data->frames_decoded += frames;

	SRC_DATA src_data;
	src_data.data_in = data->buffer;
	src_data.data_out = data->pcm;
	src_data.input_frames = frames;
	src_data.output_frames = data->pcm_frames;
	src_data.src_ratio = data->resample_ratio;
	src_data.end_of_input = (data->frames_decoded >= data->total_audio_frames);
	
	int src_error = src_process(data->src, &src_data);
	if(src_error != 0)
		return SRC_ERROR_START + src_error;

	//TODO: Check src_data.input_frames_used
	data->pcm_left = src_data.output_frames_gen;

	return MusicPlayNoError;
}

void stream_write (struct SoundIoOutStream* stream, int frame_count_min, int frame_count_max)
{
	struct SoundIoChannelArea* areas;
	MusicPlayStreamData* data = stream->userdata;
	int error, src_error;

	int frames_left = frame_count_max;
	while(frames_left > 0)
	{
		int current_frames = frames_left;
		error = soundio_outstream_begin_write(stream, &areas, &current_frames);
		if(error != SoundIoErrorNone && error != SoundIoErrorUnderflow)
		{
			//TODO: Check error in stream_write
		}

		for(int frame = 0; frame < current_frames; frame++)
		{
			if(((data->pcm_left <= 0 && !data->end_of_file) || data->time_to_seek > 0) && get_pcm_and_resample(data, stream->sample_rate) != 0)
			{
				//TODO: Check src_error in stream_write
			}

			if(data->end_of_file)
			{
				for(int channel = 0; channel < stream->layout.channel_count; channel++)
					data->convert_format(areas[channel].ptr + areas[channel].step * frame, 0.0); //write silent to each channel
			}
			else if(stream->layout.channel_count == 2) //stereo
			{
				data->convert_format(areas[0].ptr + areas[0].step * frame, data->pcm[data->pcm_start * 2]);
				data->convert_format(areas[1].ptr + areas[1].step * frame, data->pcm[data->pcm_start * 2 + 1]);
				data->pcm_start++;
				data->pcm_left--;
			}
			else //stream is not stereo, downmix to mono and send to each channel
			{
				float downmix = (data->pcm[data->pcm_start * 2] + data->pcm[data->pcm_start * 2 + 1]) / 2.0f;
				for(int channel = 0; channel < stream->layout.channel_count; channel++)
					data->convert_format(areas[channel].ptr + areas[channel].step * frame, downmix);
				data->pcm_start++;
				data->pcm_left--;
			}
		}

		error = soundio_outstream_end_write(stream);
		if(error != SoundIoErrorNone && error != SoundIoErrorUnderflow)
		{
			//TODO: check FATAL stream error in stream_write
		}

		frames_left -= current_frames;
		data->last_write_time = monotonic_time();
		data->frames_played += current_frames;
	}
}

void stream_underflow (struct SoundIoOutStream* stream)
{
	fprintf(stderr, "stream_underflow!\n");
	//TODO: stream_underflow
}

void stream_error (struct SoundIoOutStream* stream, int error)
{
	fprintf(stderr, "stream_error: %s\n", soundio_strerror(error));
	//TODO: stream_error
}

void convert_sample_f32ne (void* value, float pcm)
{
	float* out = (float*)value;
	*out = pcm;
}

void convert_sample_s32ne (void* value, float pcm)
{
	int32_t* out = (int32_t*)value;
	double range = ((double)INT32_MAX - (double)INT32_MIN) / 2.0;
	*out = (int32_t)(pcm * range);
}

void convert_sample_s16ne (void* value, float pcm)
{
	int16_t* out = (int16_t*)value;
	double range = ((double)INT16_MAX - (double)INT16_MIN) / 2.0;
	*out = (int16_t)(pcm * range);
}

void convert_sample_u8 (void* value, float pcm)
{
	uint8_t* out = (uint8_t*)value;
	*out = (uint8_t)((pcm + 1.0) * (float)UINT8_MAX / 2.0);
}

convert_format_callback get_convert_callback (struct SoundIoDevice* device, enum SoundIoFormat* format, int* error)
{
	*error = MusicPlayNoError;

	if(soundio_device_supports_format(device, SoundIoFormatFloat32NE))
	{
		*format = SoundIoFormatFloat32NE;
		return convert_sample_f32ne;
	}
	if(soundio_device_supports_format(device, SoundIoFormatS32NE))
	{
		*format = SoundIoFormatS32NE;
		return convert_sample_s32ne;
	}
	if(soundio_device_supports_format(device, SoundIoFormatS16NE))
	{
		*format = SoundIoFormatS16NE;
		return convert_sample_s16ne;
	}
	if(soundio_device_supports_format(device, SoundIoFormatU8))
	{
		*format = SoundIoFormatU8;
		return convert_sample_u8;
	}
	
	*error = MusicPlayInvalidFormat;
	return NULL;
}

decoder_init_callback get_decoder_init_callback (const char* filename)
{
	const char* extension = get_extension(filename);

	if(compare_insensetive(extension, WAV_EXTENSION))
		return decoder_wav_init;

#ifdef MUSICPLAY_OPUS_ENABLE
	if(compare_insensetive(extension, OPUS_EXTENSION))
		return decoder_opus_init;
#endif

#ifdef MUSICPLAY_MP3_ENABLE
	if(compare_insensetive(extension, MP3_EXTENSION))
		return decoder_mp3_init;
#endif
	
	return NULL;
}

decoder_destroy_callback get_decoder_destroy_callback (MusicPlayDecoderType type)
{
	switch(type)
	{
		case MusicPlayWav:
			return decoder_wav_destroy;

#ifdef MUSICPLAY_OPUS_ENABLE
		case MusicPlayOpus:
			return decoder_opus_destroy;
#endif

#ifdef MUSICPLAY_MP3_ENABLE
		case MusicPlayMp3:
			return decoder_mp3_destroy;
#endif

		default:
			return NULL;
	}
}

void samplerate_init (MusicPlayData* data, int converter_type, int* error)
{
	int src_error;
	data->stream_data.src = src_new(converter_type, 2, &src_error);
	if(data->stream_data.src == NULL)
		*error = src_error + SRC_ERROR_START;
	else
		*error = MusicPlayNoError;	
}