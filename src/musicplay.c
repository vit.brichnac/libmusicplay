#include <stdlib.h>
#include <soundio.h>
#include "types.h"
#include "audioprocess.h"
#include "tools.h"
#include "config.h"

#include <stdio.h>

#define SYNC_TRESHOLD 0.015

const char* musicplay_version (int* major, int* minor, int* patch)
{
	if(major != NULL)
		*major = MUSICPLAY_VERSION_MAJOR;
	if(minor != NULL)
		*minor = MUSICPLAY_VERSION_MINOR;
	if(patch != NULL)
		*patch = MUSICPLAY_VERSION_PATCH;
	return MUSICPLAY_VERSION;
}

const char* musicplay_strerror (int error)
{
	if(error >= WAV_ERROR_START) //Decoder error
		return musicplay_decoder_strerror(error);
	if(error >= SRC_ERROR_START)
		return src_strerror(error - SRC_ERROR_START);
	if(error >= SOUNDIO_ERROR_START)
		return soundio_strerror(error - SOUNDIO_ERROR_START);
	
	switch(error)
	{
		case MusicPlayNoError:
			return "Success";
		case MusicPlayInvalidAlloc:
			return "Error with allocation libmusicplay variables";
		case MusicPlayNotPrepared:
			return "Function is run before setup required component";
		case MusicPlayInvalidFormat:
			return "Device doesn't support any compatible format";
		case MusicPlayInvalidFile:
			return "File has unsupported format or it is broken";
		case MusicPlayNoAudio:
			return "No audio loaded";
		case MusicPlayFileNotExists:
			return "Trying load audio from file, that doesn't exists";
		default:
			return "Invalid error number";
	}
}

void musicplay_unload_audio (MusicPlayData* data)
{
	if(data->stream_data.filedata == NULL) //audio is not loaded
		return;

	decoder_destroy_callback destroy = get_decoder_destroy_callback(data->stream_data.decoder_type);
	destroy(&data->stream_data);

	free(data->stream_data.filedata);
	data->stream_data.filedata = NULL;
	data->stream_data.frames_played = 0;
	data->stream_data.end_of_file = false;

	data->stream_data.src = src_delete(data->stream_data.src);
}

void musicplay_destroy_stream (MusicPlayData* data)
{
	if(data->stream_data.filedata != NULL)
		musicplay_unload_audio(data);

	if(data->stream != NULL)
	{
		soundio_outstream_destroy(data->stream);
		data->stream_data.convert_format = NULL;
	}
}

void musicplay_destroy_device (MusicPlayData* data)
{
	if(data->stream != NULL)
		musicplay_destroy_stream(data);
	if(data->device != NULL)
		soundio_device_unref(data->device);
}

void musicplay_destroy (MusicPlayData* data)
{
	if(data->device != NULL)
		musicplay_destroy_device(data);
	if(data->soundio != NULL)
		soundio_destroy(data->soundio);
	free(data);
}

MusicPlayData* musicplay_init (int* error)
{
	*error = MusicPlayNoError;
	MusicPlayData* data = calloc(1, sizeof(MusicPlayData));
	if((data->soundio = soundio_create()) == NULL)
	{
		*error = MusicPlayInvalidAlloc;
		return NULL;
	}
	return data;
}

bool musicplay_check_backend (enum SoundIoBackend backend)
{
	return soundio_have_backend(backend);
}

void musicplay_init_backend (MusicPlayData* data, enum SoundIoBackend backend, int* error)
{
	*error = MusicPlayNoError;
	if(!check_prepared(data->soundio, error))
		return;
	
	enum SoundIoError err = soundio_connect_backend(data->soundio, backend);
	if(err != SoundIoErrorNone)
	{
		*error = err + SOUNDIO_ERROR_START;
		soundio_connect_backend(data->soundio, SoundIoBackendNone);
	}
}

int musicplay_get_device_count (MusicPlayData* data, int* error)
{
	*error = MusicPlayNoError;
	if(!check_prepared(data->soundio, error))
		return 0;
	soundio_flush_events(data->soundio);
	return soundio_output_device_count(data->soundio);
}

int musicplay_get_default_output_device_index (MusicPlayData* data)
{
	return soundio_default_output_device_index(data->soundio);
}

MusicPlayDeviceInfo musicplay_get_device_info (MusicPlayData* data, int index, int* error)
{
	MusicPlayDeviceInfo output;
	output.index = -1; //for invalid return

	if(!check_prepared(data->soundio, error))
		return output;

	soundio_flush_events(data->soundio);
	struct SoundIoDevice* device = soundio_get_output_device(data->soundio, index);
	if(*error != SoundIoErrorNone)
		*error = SOUNDIO_ERROR_START + device->probe_error;

	output.index = index;
	output.id = duplicate_string(device->id);
	output.name = duplicate_string(device->name);
	output.is_raw = device->is_raw;
	output.software_latency_min = device->software_latency_min;
	output.software_latency_max = device->software_latency_max;
	output.samplerate_min = -1;
	output.samplerate_max = -1;
	for(int i = 0; i < device->sample_rate_count; i++)
	{
		struct SoundIoSampleRateRange range = device->sample_rates[i];
		if(output.samplerate_min <= 0 || output.samplerate_min > range.min)
			output.samplerate_min = range.min;
		if(output.samplerate_max <= 0 || output.samplerate_max < range.max)
			output.samplerate_max = range.max;
	}
	soundio_device_unref(device);
	return output;
}

void musicplay_free_device_info (MusicPlayDeviceInfo info)
{
	if(info.id != NULL)
	{
		free(info.id);
		info.id = NULL;
	}
	if(info.name != NULL)
	{
		free(info.name);
		info.name = NULL;
	}
}

void musicplay_init_device (MusicPlayData* data, int index, int* error)
{
	if(!check_prepared(data->soundio, error))
		return;
	data->device = soundio_get_output_device(data->soundio, index);
	if(data->device == NULL)
		*error = MusicPlayInvalidAlloc;
	else if(data->device->probe_error != SoundIoErrorNone)
		*error = data->device->probe_error + SOUNDIO_ERROR_START;
	else
		*error = MusicPlayNoError;
}

void musicplay_init_stream (MusicPlayData* data, const char* name, int samplerate, double software_latency, int* error)
{
	*error = MusicPlayNoError;
	if(!check_prepared(data->device, error))
		return;
	data->stream = soundio_outstream_create(data->device);
	if(data->stream == NULL)
	{
		*error = MusicPlayInvalidAlloc;
		return;
	}

	data->stream->name = name;
	data->stream->sample_rate = samplerate;
	data->stream->software_latency = software_latency;
	data->stream->userdata = &data->stream_data;
	data->stream->write_callback = stream_write;
	data->stream->underflow_callback = stream_underflow;
	data->stream->error_callback = stream_error;

	data->stream_data.convert_format = get_convert_callback(data->device, &data->stream->format, error);
	if(*error != MusicPlayNoError)
		return;

	data->stream_data.paused = true;
	data->stream_data.time_to_seek = -1;
	
	int errorio = soundio_outstream_open(data->stream);
	if(errorio != SoundIoErrorNone)
		*error = errorio + SOUNDIO_ERROR_START;
}

void musicplay_load_audio (MusicPlayData* data, const char* filename, int converter_type, int* error)
{
	*error = MusicPlayNoError;
	if(data->stream_data.filedata != NULL) //Unload old audio
		musicplay_unload_audio(data);

	data->stream_data.filedata = load_whole_file(filename, &data->stream_data.filesize, error);
	if(*error != MusicPlayNoError)
		return;

	decoder_init_callback init = get_decoder_init_callback(filename);
	if(init == NULL)
	{
		*error = MusicPlayInvalidFile;
		return;
	}

	samplerate_init(data, converter_type, error);

	init(&data->stream_data, data->stream->sample_rate, error);
	if(*error != MusicPlayNoError)
		return;

	get_pcm_and_resample(&data->stream_data); //load first frames
}

void musicplay_play (MusicPlayData* data, int* error)
{
	*error = MusicPlayNoError;
	if(!check_prepared(data->stream, error))
		return;
	data->stream_data.last_write_time = monotonic_time();
	int errorio = soundio_outstream_start(data->stream);
	data->stream_data.paused = false;
	if(errorio != SoundIoErrorNone)
		*error = errorio + SOUNDIO_ERROR_START;
}

void musicplay_pause (MusicPlayData* data, bool pause, int* error)
{
	*error = MusicPlayNoError;
	if(!check_prepared(data->stream, error))
		return;
	if(!pause) //restore time offset
		data->stream_data.last_write_time = monotonic_time() + data->stream_data.last_write_time;
	int errorio = soundio_outstream_pause(data->stream, pause);
	if(pause) //save time offset
		data->stream_data.last_write_time = monotonic_time() - data->stream_data.last_write_time;
	if(errorio != SoundIoErrorNone)
		*error = errorio + SOUNDIO_ERROR_START;
	data->stream_data.paused = pause;
}

double musicplay_current_audio_time (MusicPlayData* data, int* error)
{
	*error = MusicPlayNoError;
	if(!check_prepared(data->stream, error) || !check_prepared(data->stream_data.filedata, error))
		return -1.0;

	if(data->stream_data.paused)
		return (double)data->stream_data.frames_played / (double)data->stream->sample_rate - data->stream->software_latency + (double)(data->stream_data.last_write_time) / 1000.0;
	uint64_t current_time = monotonic_time();
	return (double)data->stream_data.frames_played / (double)data->stream->sample_rate - data->stream->software_latency + (double)(current_time - data->stream_data.last_write_time) / 1000.0;
}

double musicplay_total_audio_time (MusicPlayData* data, int* error)
{
	*error = MusicPlayNoError;
	if(!check_prepared(data->stream, error) || !check_prepared(data->stream_data.filedata, error))
		return -1.0;
	
	return (double)data->stream_data.total_audio_frames / (double)data->stream_data.samplerate;
}

bool musicplay_end_of_file (MusicPlayData* data, int* error)
{
	double current_time = musicplay_current_audio_time(data, error);
	if(*error != MusicPlayNoError)
		return true;
	double total_time = musicplay_total_audio_time(data, error);
	if(*error != MusicPlayNoError)
		return true;

	return current_time >= total_time;
}

void musicplay_seek (MusicPlayData* data, double time, int* error)
{
	if(!check_prepared(data->stream, error) || !check_prepared(data->stream_data.filedata, error))
		return;

	data->stream_data.seek_request_time = monotonic_time();
	data->stream_data.time_to_seek = time;
	*error = soundio_outstream_clear_buffer(data->stream);
	if(*error != SoundIoErrorNone)
		*error += SOUNDIO_ERROR_START;
	else
		*error = MusicPlayNoError;
}

void musicplay_sync (MusicPlayData* data, double time, int* error)
{
	*error = MusicPlayNoError;
	double diff = time - musicplay_current_audio_time(data, error);
	if(*error != MusicPlayNoError)
		return;
	
	if(diff > SYNC_TRESHOLD || diff < SYNC_TRESHOLD)
		musicplay_seek(data, time, error);
}