#include <stdlib.h>
#include "../types.h"

#define MINIMP3_FLOAT_OUTPUT
#define MINIMP3_IMPLEMENTATION
#include "minimp3.h"
#include "minimp3_ex.h"

#define MP3_BUFFER_LENGTH (20)

const char* decoder_mp3_strerror (int error)
{
	error = MP3_ERROR_START - error;
	switch (error)
	{
		case MP3D_E_PARAM:
			return "MP3D_E_PARAM";
		case MP3D_E_MEMORY:
			return "MP3D_E_MEMORY";
		case MP3D_E_IOERROR:
			return "MP3D_E_IOERROR";
		case MP3D_E_USER:
			return "MP3D_E_USER";
		case MP3D_E_DECODE:
			return "MP3D_E_DECODE";
		default:
			return "Unknown error";
	}
}

void decoder_mp3_destroy (MusicPlayStreamData* data)
{
	data->get_buffer_pcm = NULL;
	data->audio_seek = NULL;

	free(data->buffer);
	data->buffer_frames = 0;
	free(data->pcm);
	data->pcm_frames = 0;
	data->pcm_start = 0;
	data->pcm_left = 0;

	data->total_audio_frames = 0;
	data->frames_decoded = 0;

	mp3dec_ex_close((mp3dec_ex_t*)data->decoder);
}

long decoder_mp3_get_pcm (void* decoder, float* buffer, long buffer_frames, int* error)
{
	*error = MusicPlayNoError;

	mp3dec_ex_t* dec = (mp3dec_ex_t*)decoder;
	size_t readed = mp3dec_ex_read(dec, (mp3d_sample_t*)buffer, buffer_frames);

	if(dec->info.channels == 1) //mono, copy channel
	{
		for(long i = buffer_frames - 1; i >= 0; i--)
			buffer[i * 2] = buffer[i];
		for(long i = 0; i < buffer_frames; i++)
			buffer[i * 2 + 1] = buffer[i * 2];
	}

	if(readed != buffer_frames && dec->last_error)
	{
		*error = MP3_ERROR_START - dec->last_error;
		return 0;
	}
	
	return readed / dec->info.channels;
}

void decoder_mp3_seek (void* decoder, long frame, int* error)
{
	*error = MusicPlayNoError;
	int dec_error;
	if(dec_error = mp3dec_ex_seek((mp3dec_ex_t*)decoder, frame))
		*error = MP3_ERROR_START - dec_error;
}

void decoder_mp3_init (MusicPlayStreamData* data, int stream_samplerate, int* error)
{
	*error = MusicPlayNoError;
	if(data->filedata == NULL)
	{
		*error = MusicPlayNotPrepared;
		return;
	}

	data->decoder_type = MusicPlayMp3;
	mp3dec_ex_t* decoder = (mp3dec_ex_t*)malloc(sizeof(mp3dec_ex_t));
	data->decoder = (void*)decoder;

	int decoder_error = 0;
	if(decoder_error = mp3dec_ex_open_buf(decoder, data->filedata, data->filesize, MP3D_SEEK_TO_SAMPLE))
	{
		*error = MP3_ERROR_START - decoder_error; //minimp3 has negative error numbers
		return;
	}

	data->samplerate = decoder->info.hz;
	data->total_audio_frames = decoder->samples / decoder->info.channels;
	data->resample_ratio = (double)stream_samplerate / (double)data->samplerate;

	if(decoder->info.channels != 2 && decoder->info.channels != 1) //Not mono or stereo
	{
		*error = MusicPlayInvalidFile;
		mp3dec_ex_close(data->decoder);
	}

	long buffer_frames = MP3_BUFFER_LENGTH * data->samplerate / 1000;

	data->buffer_frames = buffer_frames;
	data->buffer = malloc(sizeof(float) * data->buffer_frames * 2);
	if(data->buffer == NULL)
	{
		*error = MusicPlayInvalidAlloc;
		mp3dec_ex_close(data->decoder);
		return;
	}

	data->pcm_frames = stream_samplerate * buffer_frames / 1000;
	data->pcm_start = 0;
	data->pcm_left = 0;
	data->pcm = malloc(sizeof(float) * data->pcm_frames * 2);
	if(data->pcm == NULL)
	{
		*error = MusicPlayInvalidAlloc;
		mp3dec_ex_close(data->decoder);
		free(data->buffer);
		return;
	}

	data->frames_played = 0;
	data->end_of_file = false;

	data->frames_decoded = 0;

	data->get_buffer_pcm = decoder_mp3_get_pcm;
	data->audio_seek = decoder_mp3_seek;
}