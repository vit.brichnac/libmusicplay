#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "../types.h"
#include "../tools.h"
#include "decoder_wav.h"

#define WAV_GLOBAL_CHUNK_ID "RIFF"
#define WAV_GLOBAL_CHUNK_ID_LENGTH 4
#define WAV_FORMAT "WAVE"
#define WAV_FORMAT_LENGTH "WAVE"
#define WAV_SUBCHUNK1_ID "fmt "
#define WAV_SUBCHUNK1_ID_LENGTH 4
#define WAV_SUBCHUNK2_ID "data"
#define WAV_SUBCHUNK2_ID_LENGTH 4

#define WAV_PCM 1
#define WAV_FLOAT 3

#define WAV_BUFFER_LENGTH 25

struct internal_wav_struct
{
	char global_chunk_id[4];
	int32_t chunk_size;
	char format[4];

	char subchunk1_id[4];
	int32_t subchunk1_size;
	int16_t audioformat;
	int16_t channels;
	int32_t samplerate;
	int32_t byterate;
	int16_t block_align;
	int16_t bits_per_sample;
	
	char subchunk2_id[4];
	int32_t subchunk2_size;
};

static void check_endian (struct internal_wav_struct* wav)
{
	wav->chunk_size = LITTLE_ENDIAN_32(wav->chunk_size);

	wav->subchunk1_size = LITTLE_ENDIAN_32(wav->subchunk1_size);
	wav->audioformat = LITTLE_ENDIAN_16(wav->audioformat);
	wav->channels = LITTLE_ENDIAN_16(wav->channels);
	wav->samplerate = LITTLE_ENDIAN_32(wav->samplerate);
	wav->byterate = LITTLE_ENDIAN_32(wav->byterate);
	wav->block_align = LITTLE_ENDIAN_16(wav->block_align);
	wav->bits_per_sample = LITTLE_ENDIAN_16(wav->bits_per_sample);

	wav->subchunk2_size = LITTLE_ENDIAN_32(wav->subchunk2_size);
}

static float wav_read_float (const char* data, size_t* position)
{
	float* in = (float*)(data + *position);
	*position += sizeof(float);
	return *in;
}

static float wav_read_u8 (const char* data, size_t* position)
{
	uint8_t* in = (uint8_t*)(data + *position);
	*position += sizeof(uint8_t);
	return (*in * (float)UINT8_MAX - 1.0) / 2.0;
}

static float wav_read_s16 (const char* data, size_t* position)
{
	int16_t* in = (int16_t*)(data + *position);
	*position += sizeof(int16_t);
	double range = ((double)INT16_MAX - (double)INT16_MIN) / 2.0;
	return *in / range;
}

static float wav_read_s24 (const char* data, size_t* position)
{
	uint32_t buffer = (*((uint32_t*)(data + *position)) & 0x00FFFFFF) << 8;
	int32_t* in = (int32_t*)&buffer;
	*position += 3;
	double range = ((double)INT32_MAX - (double)INT32_MIN) / 2.0;
	return *in / range;
}

static float wav_read_s32 (const char* data, size_t* position)
{
	int32_t* in = (int32_t*)(data + *position);
	*position += sizeof(int32_t);
	double range = ((double)INT32_MAX - (double)INT32_MIN) / 2.0;
	return *in / range;
}

static wav_reader internal_wav_get_reader(int16_t audioformat, int16_t bits_per_sample)
{
	if(audioformat == WAV_FLOAT && bits_per_sample == 32)
		return wav_read_float;
	if(audioformat == WAV_PCM)
	{
		switch (bits_per_sample)
		{
			case 8:
				return wav_read_u8;
			case 16:
				return wav_read_s16;
			case 24:
				return wav_read_s24;
			case 32:
				return wav_read_s32;
			default:
				return NULL;
		}
	}
	return NULL;
}

const char* decoder_wav_strerror (int error)
{
	switch (error - WAV_ERROR_START)
	{
		case WavBrokenFile:
			return "File is broken (unexpected data in header)";
		case WavInvalidAudioFormat:
			return "File has unsupported audio format";
		case WavTooManyChannels:
			return "File has too many channels (more then stereo)";
		case WavOutOfFile:
			return "Seeked position is out of file";
		case WavNoError:
			return "No error";
		default:
			return "Invalid WAV error number";
	}
}

void decoder_wav_destroy (MusicPlayStreamData* data)
{
	data->get_buffer_pcm = NULL;
	data->audio_seek = NULL;

	free(data->buffer);
	data->buffer_frames = 0;
	free(data->pcm);
	data->pcm_frames = 0;
	data->pcm_start = 0;
	data->pcm_left = 0;

	data->total_audio_frames = 0;
	data->frames_decoded = 0;

	free(data->decoder);
}

long decoder_wav_get_pcm (void* decoder, float* buffer, long buffer_frames, int* error)
{
	*error = MusicPlayNoError;

	WavStruct* wav = (WavStruct*)decoder;

	if(wav->current_pos >= wav->datasize) //End of file
		return 0;

	for(long i = 0; i < buffer_frames; i++)
	{
		float first = wav->reader(wav->data, &wav->current_pos);
		buffer[i * 2] = first;
		buffer[i * 2 + 1] = wav->channels == 1 ? first : wav->reader(wav->data, &wav->current_pos);

		if(wav->current_pos >= wav->datasize) //End of file
			return i + 1;
	}

	return buffer_frames;
}

void decoder_wav_seek (void* decoder, long frame, int* error)
{
	*error = MusicPlayNoError;
	WavStruct* wav = (WavStruct*)decoder;
	wav->current_pos = (wav->channels * wav->bits_per_sample / 8) * frame;
	if(wav->current_pos >= wav->datasize)
	{
		*error = WavOutOfFile + WAV_ERROR_START;
		wav->current_pos = wav->datasize;
	}
}

static WavStruct* internal_wav_init (char* file, size_t filesize, int* samplerate, long* total_samples, int* error)
{
	struct internal_wav_struct* header = (struct internal_wav_struct*)file;
	check_endian(header);
	
	if(strncmp(header->global_chunk_id, WAV_GLOBAL_CHUNK_ID, WAV_GLOBAL_CHUNK_ID_LENGTH) != 0 || strncmp(header->subchunk1_id, WAV_SUBCHUNK1_ID, WAV_SUBCHUNK1_ID_LENGTH) != 0 || strncmp(header->subchunk2_id, WAV_SUBCHUNK2_ID, WAV_SUBCHUNK2_ID_LENGTH) != 0) //Check header strings
	{
		*error = WavBrokenFile;
		return NULL;
	}

	if(header->chunk_size != filesize - 8 || header->subchunk1_size != 16) //Check file size
	{
		*error = WavBrokenFile;
		return NULL;
	}

	if(header->channels != 1 && header->channels != 2) //Check channel count
	{
		*error = WavTooManyChannels;
		return NULL;
	}

	if(header->audioformat != WAV_PCM && header->audioformat != WAV_FLOAT) //Check supported format
	{
		*error = WavInvalidAudioFormat;
		return NULL;
	}

	if(header->samplerate <= 0 || header->bits_per_sample <= 0 || header->bits_per_sample > 32 || header->bits_per_sample % 8 != 0) //Check sample rate and bits per sample
	{
		*error = WavBrokenFile;
		return NULL;
	}

	if(header->block_align != header->channels * header->bits_per_sample / 8 || header->byterate != header->block_align * header->samplerate) //Check computed variables
	{
		*error = WavBrokenFile;
		return NULL;
	}

	wav_reader reader = internal_wav_get_reader(header->audioformat, header->bits_per_sample);
	if(reader == NULL)
	{
		*error = WavInvalidAudioFormat;
		return NULL;
	}

	*total_samples = header->subchunk2_size / header->block_align;
	*samplerate = header->samplerate;

	WavStruct* output = malloc(sizeof(WavStruct));
	if(output == NULL) //Invalid alloc
		return NULL;

	output->data = file + sizeof(WavStruct);
	output->datasize = header->subchunk2_size;
	output->current_pos = 0;
	output->channels = header->channels;
	output->bits_per_sample = header->bits_per_sample;
	output->reader = reader;
	return output;
}

void decoder_wav_init (MusicPlayStreamData* data, int stream_samplerate, int* error)
{
	*error = MusicPlayNoError;
	if(!check_prepared(data->filedata, error))
		return;

	data->decoder_type = MusicPlayWav;
	data->decoder = internal_wav_init(data->filedata, data->filesize, &data->samplerate, &data->total_audio_frames, error);
	if(*error != WavNoError)
	{
		*error += WAV_ERROR_START;
		return;
	}

	data->resample_ratio = (double)stream_samplerate / (double)data->samplerate;

	data->buffer_frames = data->samplerate * WAV_BUFFER_LENGTH / 1000;
	data->buffer = malloc(sizeof(float) * data->buffer_frames * 2);
	if(data->buffer == NULL)
	{
		*error = MusicPlayInvalidAlloc;
		free(data->decoder);
		return;
	}

	data->pcm_frames = stream_samplerate * WAV_BUFFER_LENGTH / 1000;
	data->pcm_start = 0;
	data->pcm_left = 0;
	data->pcm = malloc(sizeof(float) * data->pcm_frames * 2);
	if(data->pcm == NULL)
	{
		*error = MusicPlayInvalidAlloc;
		free(data->decoder);
		free(data->buffer);
		return;
	}

	data->frames_played = 0;
	data->end_of_file = false;
	data->frames_decoded = 0;
	data->get_buffer_pcm = decoder_wav_get_pcm;
	data->audio_seek = decoder_wav_seek;
}