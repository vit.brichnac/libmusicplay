#ifndef __DECODER_MP3_H__
#define __DECODER_MP3_H__

#include "../types.h"

#define MP3_EXTENSION "mp3"

const char* decoder_mp3_strerror (int error);

void decoder_mp3_destroy (MusicPlayStreamData* data);

long decoder_mp3_get_pcm (void* decoder, float* buffer, long buffer_frames, int* error);

void decoder_mp3_seek (void* decoder, long frame, int* error);

void decoder_mp3_init (MusicPlayStreamData* data, int stream_samplerate, int* error);

#endif