#ifndef __DECODER_WAV_H__
#define __DECODER_WAV_H__

#include "../types.h"

#define WAV_EXTENSION "wav"

typedef enum wav_error
{
	WavNoError,
	WavBrokenFile,
	WavInvalidAudioFormat,
	WavTooManyChannels,
	WavOutOfFile
} WavError;

typedef float (*wav_reader)(const char* data, size_t* position);

typedef struct wav_struct
{
	const char* data;
	size_t datasize;

	int channels;
	int bits_per_sample;
	//Position (bytes from start) of next sample
	size_t current_pos;

	wav_reader reader;
} WavStruct;

const char* decoder_wav_strerror (int error);

void decoder_wav_destroy (MusicPlayStreamData* data);

long decoder_wav_get_pcm (void* decoder, float* buffer, long buffer_frames, int* error);

void decoder_wav_seek (void* decoder, long frame, int* error);

void decoder_wav_init (MusicPlayStreamData* data, int stream_samplerate, int* error);

#endif