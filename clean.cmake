# Remove all files in build directory and build directories in submodules

MACRO(REMOVE_DIR PATH)
	FILE(GLOB FILES_TO_REMOVE LIST_DIRECTORIES true "${CMAKE_SOURCE_DIR}/${PATH}/*")
	IF(FILES_TO_REMOVE)
		STRING (REPLACE ";" "\n\t" FILES_TO_REMOVE_LOG "${FILES_TO_REMOVE}")
		MESSAGE("Removing files from ${PATH}:\n\t${FILES_TO_REMOVE_LOG}")
		FILE(REMOVE_RECURSE ${FILES_TO_REMOVE})
	ENDIF()
ENDMACRO()

REMOVE_DIR("build")
REMOVE_DIR("submodules/libsoundio/build")
REMOVE_DIR("submodules/libsamplerate/build")
REMOVE_DIR("submodules/libopusfile/build")

# Reset all changes in submodules
FIND_PACKAGE(Git)
IF(GIT_FOUND AND EXISTS "${CMAKE_SOURCE_DIR}/.git")
	MESSAGE("Checkout all git submodules")
	EXECUTE_PROCESS(COMMAND ${GIT_EXECUTABLE} submodule foreach "\"${GIT_EXECUTABLE}\" checkout -- ."
                    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
ENDIF()